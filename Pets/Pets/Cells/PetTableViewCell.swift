//
//  PetTableViewCell.swift
//  Pets
//
//  Created by Vishal Patel on 07/12/22.
//

import Foundation
import UIKit

class PetTableViewCell: UITableViewCell {
    
    @IBOutlet weak var imgPet: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
}
