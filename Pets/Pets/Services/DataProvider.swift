//
//  DataProvider.swift
//  Pets
//
//  Created by Vishal Patel on 07/12/22.
//

import Foundation

protocol DataProviderProtocol {
    func getDataFrom<T:Decodable>(fileName: String, resultType: T.Type) -> T?
}

class DataProvider: DataProviderProtocol {
    
    func getDataFrom<T>(fileName: String, resultType: T.Type) -> T? where T : Decodable {
        if let path = Bundle.main.path(forResource: fileName, ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                let result = try? JSONDecoder().decode(resultType.self, from: data)
                return result
            } catch {
                print("error occured while decoding = \(error.localizedDescription)")
            }
        }
        return nil
    }

}
