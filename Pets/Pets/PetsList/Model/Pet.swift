//
//  Pet.swift
//  Pets
//
//  Created by Vishal Patel on 07/12/22.
//

import Foundation

struct PetsList: Decodable {
    let pets: [Pet]
}

struct Pet: Decodable {
    let image_url: String
    let title: String
    let content_url: String
    let date_added: String
}
