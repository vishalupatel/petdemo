//
//  PetsListViewController.swift
//  Pets
//
//  Created by Vishal Patel on 07/12/22.
//

import UIKit
import Kingfisher

class PetsListViewController: UIViewController {
    
    @IBOutlet weak var tblPets: UITableView!
    var viewModel: PetsListViewModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel = PetsListViewModel(data: DataProvider())
    }

}

extension PetsListViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel?.numberOfSections ?? 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel?.numberOfRowsInSection(section) ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "PetCell", for: indexPath) as? PetTableViewCell else {
            fatalError("PetCell not found")
        }
        
        let petViewModel = viewModel?.petAtIndex(indexPath.row)
        cell.lblTitle.text = petViewModel?.title ?? ""
        
        if let imageUrl = petViewModel?.imageUrl {
            cell.imgPet.kf.setImage(with: imageUrl)
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyBoard = UIStoryboard.init(name: "Main", bundle: nil)
        let controller = storyBoard.instantiateViewController(withIdentifier: "PetDetails") as! PetDetailsViewController
        controller.petViewModel = viewModel?.petAtIndex(indexPath.row)
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
}
