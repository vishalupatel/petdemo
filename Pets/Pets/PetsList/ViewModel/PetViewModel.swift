//
//  PetViewModel.swift
//  Pets
//
//  Created by Vishal Patel on 07/12/22.
//

import Foundation


struct PetsListViewModel {
    private var pets: [Pet]?
    
    init(data: DataProviderProtocol) {
        let result = data.getDataFrom(fileName: "pets_list", resultType: PetsList.self)
        self.pets = result?.pets
    }
    
    var numberOfSections: Int {
        return 1
    }
    
    func numberOfRowsInSection(_ section: Int) -> Int {
        return self.pets?.count ?? 0
    }
    
    func petAtIndex(_ index: Int) -> PetViewModel? {
        if let pet =  self.pets?[index] {
            return PetViewModel(pet: pet)
        }
        return nil
    }
    
}

struct PetViewModel {
    private let pet: Pet
    
    init(pet: Pet) {
        self.pet = pet
    }
    
    var imageUrl: URL? {
        return URL(string: self.pet.image_url) ?? nil
    }
    
    var title: String {
        return self.pet.title.capitalized
    }
    
    var dateAdded: String {
        let date = Utility.date(fromString: self.pet.date_added, formate: "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
        if let date = date {
            return Utility.string(fromDate: date, formate: "yyyy-MM-dd hh:mm:ss a")
        }
        return ""
    }
    
    var contentUrl: URL? {
        return URL(string: self.pet.content_url) ?? nil
    }
    
}
