//
//  Utility.swift
//  Pets
//
//  Created by Vishal Patel on 07/12/22.
//

import Foundation

class Utility {
    class func date(fromString stringDate: String, formate: String) -> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = formate
        return dateFormatter.date(from: stringDate)
    }
    
    class func string(fromDate date: Date, formate: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = formate
        return dateFormatter.string(from: date)
    }
}
