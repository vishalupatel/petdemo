//
//  PetDetailsViewController.swift
//  Pets
//
//  Created by Vishal Patel on 07/12/22.
//

import UIKit

class PetDetailsViewController: UIViewController {

    var petViewModel: PetViewModel?
    
    @IBOutlet weak var imgPet: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDateAdded: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setup()
    }
    
    func setup() {
        if let imageUrl = petViewModel?.imageUrl {
            imgPet.kf.setImage(with: imageUrl)
        }
        
        lblTitle.text = petViewModel?.title ?? ""
        lblDateAdded.text = petViewModel?.dateAdded ?? ""
    }
    
    @IBAction func btnOpenContentUrl(_ sender: Any) {
        if let contentUrl = petViewModel?.contentUrl {
            UIApplication.shared.open(contentUrl)
        }
    }
    
}
