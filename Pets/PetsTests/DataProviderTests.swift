//
//  DataProviderTests.swift
//  PetsTests
//
//  Created by Vishal Patel on 08/12/22.
//

import XCTest
@testable import Pets

final class DataProviderTests: XCTestCase {

    var sut:DataProvider?
    override func setUpWithError() throws {
        sut = DataProvider()
    }

    override func tearDownWithError() throws {
        sut = nil
    }
    
    func testDataResponse() {
        let obj = sut?.getDataFrom(fileName: "pets_list", resultType: PetsList.self)
        XCTAssertEqual(obj?.pets.isEmpty, false)
    }
}
