//
//  PetsTests.swift
//  PetsTests
//
//  Created by Vishal Patel on 07/12/22.
//

import XCTest
@testable import Pets

class MockDataProvider: DataProviderProtocol {
    func getDataFrom<T>(fileName: String, resultType: T.Type) -> T? where T : Decodable {
        let pet1 = Pet(image_url: "https://upload.wikimedia.org/wikipedia/commons/thumb/0/0b/Cat_poster_1.jpg/1200px-Cat_poster_1.jpg",
                       title: "Cat",
                       content_url: "https://en.wikipedia.org/wiki/Cat",
                       date_added: "2018-06-02T03:27:38.027Z")
        
        let pet2 = Pet(image_url: "https://upload.wikimedia.org/wikipedia/commons/3/30/RabbitMilwaukee.jpg",
                       title: "Rabbit",
                       content_url: "https://en.wikipedia.org/wiki/Rabbit",
                       date_added: "2018-06-06T08:36:16.027Z")
        
        let pets = PetsList(pets: [pet1, pet2])
        return pets as? T
    }
}

final class PetsTests: XCTestCase {

    var sut:PetsListViewModel?
    
    override func setUpWithError() throws {
        sut = PetsListViewModel(data: MockDataProvider())
    }

    override func tearDownWithError() throws {
        sut = nil
    }

    func testNumberOfSections(){
        XCTAssertEqual(sut?.numberOfSections == 1, true)
    }
    
    func testNumberOfRowsInSection(){
        XCTAssertEqual(sut?.numberOfRowsInSection(0) == 2, true)
    }
    
    func testPetAtIndex(){
        let pet = sut?.petAtIndex(0)
        XCTAssertEqual(pet?.title == "Cat", true)
        
        let imageUrl = URL(string: "https://upload.wikimedia.org/wikipedia/commons/thumb/0/0b/Cat_poster_1.jpg/1200px-Cat_poster_1.jpg")
        XCTAssertEqual(pet?.imageUrl == imageUrl, true)
        
        let contentUrl = URL(string: "https://en.wikipedia.org/wiki/Cat")
        XCTAssertEqual(pet?.contentUrl == contentUrl, true)
        
        let dateString = "2018-06-02 03:27:38 AM"
        XCTAssertEqual(pet?.dateAdded == dateString, true)
    }
    
}
